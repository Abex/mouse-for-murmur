var murmur={};
var shim=(function()
{
  callback = function(cbs)
  {
    this.callbacks=cbs;
  };
  callback.prototype.remove=function()
  {
    this.callbacks.forEach(function(e)
    {
      if(e.type=="Meta")
      {
        _murmur._removeMetaCallback(e.cb);
      }
      else if(e.type=="Server")
      {
        _murmur._removeServerCallback(e.cb,e.server);
      }
    });
  };
  server = function(server)
  {
    this.server=server;
  };
  server.prototype.getIsRunning = function()
  {
    return this.server.isRunning()==1;
  };
  server.prototype.isRunning = function()
  {
    return this.server.isRunning()==1;
  };
  server.prototype.start = function()
  {
    return this.server.start();
  };
  server.prototype.stop = function()
  {
    return this.server.stop();
  };
  server.prototype.delete = function()
  {
    return this.server.delete();
  };
  server.prototype.getId = function()
  {
    return this.server.id()+0;
  };
  server.prototype.id = function()
  {
    return this.server.id()+0;
  };
  server.prototype.getConf = function(key)
  {
    return this.server.getConf(key)+"";
  };
  server.prototype.setConf = function(key,v)
  {
    return this.server.setConf(key,v);
  };
  server.prototype.getAllConf = function()
  {
    var jConf = _murmur._getAllConfServer(this.server.id());
    var rConf={};
    jConf.foreach(new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback({callback:function(k)
    {
      rConf[k]=jConf.get(k)+"";
    },getPlugin:function(){return pluginName;}}));
    return rConf;
  };
  server.prototype.setSuperuserPassword = function(pw)
  {
    return this.server.setSuperuserPassword(pw);
  };
  server.prototype.getLogLen = function()
  {
    return this.server.getLogLen()+0;
  };
  server.prototype.getLog = function(first,last)
  {
    var jLog = this.server.getLog(first,last);
    var rLog = [];
    for(var i=0;i<jLog.length;i++)
    {
      rLog[i+0] = {
        timestamp:jLog[i].timestamp+0,
        txt:jLog[i].txt+"",
        date:new Date(jLog[i].timestamp+0),
        toString:function(){return this.date.toString()+" : "+txt;}
      };
    }
    return rLog;
  };
  server.prototype.getUsers = function()
  {
    var jLog = _murmur._getUsersServer(this.server.id());
    var rLog = [];
    jLog.foreach(new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback({callback:function(i)
    {
      rLog[i+0] = new shim.user(jLog.get(i));
    },getPlugin:function(){return pluginName;}}));
    return rLog;
  };
  server.prototype.getChannels = function()
  {
    var jLog = _murmur._getChannelsServer(this.server.id());
    var rLog = [];
    for(var i=0;i<jLog.length;i++)
    {
      rLog[i+0] = new shim.channel(jLog[i]);
    }
    return rLog;
  };
  var toTree = function(jTree)
  {
    var tree={
      c:new shim.channel(jTree.c),
      users:[],
      children:[],
    };
    for(var cUserIndex=0;cUserIndex<jTree.users.length;cUserIndex++)
    {
      tree.users[cUserIndex+0] = new shim.user(jTree.users[cUserIndex]);
    }
    for(var cChannelIndex=0;cChannelIndex<jTree.children.length;cChannelIndex++)
    {
      tree.children[cChannelIndex+0] = shim.toTree(jTree.children[cChannelIndex]);
    }
    return tree;
  };
  server.prototype.getTree = function()
  {
    var jTree = this.server.getTree();
    return shim.toTree(jTree);
  };
  ban = function(_ban)
  {
    this.ban = _ban;
  };
  server.prototype.getBans = function()
  {
    var bans={};
    var jBans = this.server.getBans();
    for(var cBanIndex=0;cBanIndex<jBans.length;cBanIndex++)
    {
      bans[cBanIndex]= new murmur.Ban(jBans[cBanIndex]);
    }
    return bans;
  };
  server.prototype.setBans = function(bans)
  {
    var mBans = [];
    bans.forEach(function(e,i)
    {
      mBans[i]=e.ban;
    });
    this.server.setBans(mBans);
  };
  server.prototype.getState = function(session)
  {
    return new shim.user(this.server.getState(session));
  };
  server.prototype.setState = function(user)
  {
    this.server.setState(user.user);
  };
  server.prototype.sendMessage = function(session,text)
  {
    this.server.sendMessage(session,text);
  };
  server.prototype.hasPermission = function(session, channelid, perm)
  {
    return this.server.hasPermission(session, channelid, perm)==1;
  };
  server.prototype.effectivePermission = function(session, channelid)
  {
    return this.server.effectivePermission(session, channelid)+0;
  };
  server.prototype.getChannelState = function(channelid)
  {
    return new shim.channel(this.server.getChannelState(channelid));
  };
  server.prototype.setChannelState = function(channel)
  {
    this.server.setChannelState(channel.channel);
  };
  server.prototype.removeChannel = function(channelid)
  {
    this.server.removeChannel(channelid);
  };
  server.prototype.addChannel = function(name,parent)
  {
    return this.serven.addChannel(name,parent)+0;
  };
  server.prototype.sendMessageChannel = function(channelid,tree,text)
  {
    this.server.sendMessageChannel(channelid,tree,text);
  };
  server.prototype.on = function(event,callback)
  {
    var events = event.split(" ");
    cb = [];
    for(var index in events)
    {
      var event = events[index];
      if(event==="userConnected")
      {
        console.log(pluginName+" has registered Server::userConnected");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.user(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerConnect(ccb,this.server.id())};
      }
      else if(event==="userDisconnected")
      {
        console.log(pluginName+" has registered Server::userDisconnected");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.user(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerDisconnect(ccb,this.server.id())};
      }
      else if(event==="userStateChanged")
      {
        console.log(pluginName+" has registered Server::userStateChanged");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.user(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerStateChanged(ccb,this.server.id())};
      }
      else if(event==="channelCreated")
      {
        console.log(pluginName+" has registered Server::channelCreated");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.channel(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerChannelCreated(ccb,this.server.id())};
      }
      else if(event==="channelRemoved")
      {
        console.log(pluginName+" has registered Server::channelRemoved");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.channel(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerChannelRemoved(ccb,this.server.id())};
      }
      else if(event==="channelStateChanged")
      {
        console.log(pluginName+" has registered Server::channelStateChanged");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
        {
          callback:function(server){return callback(new shim.channel(server));},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerChannelStateChanged(ccb,this.server.id())};
      }
      else if(event==="userTextMessage")
      {
        console.log(pluginName+" has registered Server::userTextMessage");
        var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback2Arg(
        {
          callback:function(server,text){return callback(new shim.user(server),text+"");},
          getPlugin:function()
          {
            return pluginName;
          }
        });
        cb[cb.length] = {type:"Server",server:this.server.id(),cb:_murmur._onServerUserTextMessage(ccb,this.server.id())};
      }
      else
      {
        throw "Event "+event+"is not a Server Event!";
      }
    }
    return new shim.callback(cb);
  };
  user = function(_user)
  {
    this.user = _user;
  };
  user.prototype.toJSON = function()
  {
    return "[User "+this.user.userid+"]";
  };
  channel = function(_channel)
  {
    this.channel=_channel;
  };
  channel.prototype.toJSON = function()
  {
    return "[Channel "+this.channel.id+"]";
  };
  return {
    callback:callback,
    server:server,
    user:user,
    channel:channel,
    toTree:toTree,
  };
})();
murmur.forEachServer = function(callback)
{
  return _murmur._forEachServer(new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
  {
    callback:function(server){return callback(new shim.server(server));},
    getPlugin:function()
    {
      return pluginName;
    }
  }));
};
murmur.Ban = function(_ban)
{
  this.ban = _ban
};
murmur.on = function(event,callback)
{
  var events = event.split(" ");
  cb = [];
  for(var index in events)
  {
    var event = events[index];
    if(event==="start")
    {
      console.log(pluginName+" has registered Meta::ServerStarted");
      var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
      {
        callback:function(server){return callback(new shim.server(server));},
        getPlugin:function()
        {
          return pluginName;
        }
      });
      cb[cb.length] = {type:"Meta",cb:_murmur._onStarted(ccb)};
    }
    else if(event==="stop")
    {
      console.log(pluginName+" has registered Meta::ServerStopped");
      var ccb = new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback(
      {
        callback:function(server){return callback(new shim.server(server));},
        getPlugin:function()
        {
          return pluginName;
        }
      });
      cb[cb.length] = {type:"Meta",cb:_murmur._onStopped(ccb)};
    }
    else
    {
      throw "Event "+event+"is not a Meta Event!";
    }
  }
  return new shim.callback(cb);
};
murmur._murmur = _murmur;
murmur.murmur  = ice.murmur;
murmur.getServer = function(serverId)
{
  return new shim.server(ice.murmur.getServer(serverId));
};
murmur.getVersion = function()
{
  var versionNumbers = _murmur._getVersionNumbers();
  return{
    major:versionNumbers[0]+0,
    minor:versionNumbers[1]+0,
    patch:versionNumbers[2]+0,
    text:_murmur._getVersionString()+"",
    toString:function()
    {
      return this.text;
    }
  };
};
murmur.getUptime = function()
{
  return ice.murmur.getUptime();
};
murmur.newServer = function()
{
  return new shim.server(ice.murmur.newServer());
};
murmur.getBootedServers = function()
{
  var jServers = ice.murmur.getBootedServers();
  var rets={};
  for(var cServer in jServers)
  {
    rets[cServer.id()]=new shim.server(cServer);
  }
  return rets;
};
murmur.getDefaultConf = function()
{
  var jConf = _murmur._getDefaultConf();
  var rConf={};
  jConf.foreach(new Packages.tk.mii8303.mouse.jsWrapper.IjsCallback({callback:function(k)
  {
    rConf[k]=jConf.get(k)+"";
  },getPlugin:function(){return pluginName;}}));
  return rConf;
};