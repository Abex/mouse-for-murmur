package tk.mii8303.mouse.pluginLoader;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;

import tk.mii8303.mouse.plugin.Plugin;

public class pluginLoader
{
  public static HashMap<String,Plugin> plugins = new HashMap<String,Plugin>(); 
  public static void loadPlugins(String Directory)
  {
    loadPlugins(new File(Directory));
  }
  public static void loadPlugins(File Directory)
  {
    String[] names = Directory.list();
    for(String name:names)
    {
      if(new File(Directory.getAbsolutePath()+"/"+name).isDirectory())
      {
        File plugin = new File(Directory.getAbsolutePath()+"/"+name+"/index.js");
        plugins.put(name,new Plugin(plugin,name));
      }
    }
  }
  public static void startPlugins()
  {
    for(Entry<String,Plugin> plugin : plugins.entrySet())
    {
      plugin.getValue().start();
    }
  }
}
