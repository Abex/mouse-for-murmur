package tk.mii8303.mouse.plugin;

import java.io.File;
import java.io.IOException;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.RhinoException;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import tk.mii8303.mouse.http.JsHttp;
import tk.mii8303.mouse.jsWrapper.JsMouse;
import tk.mii8303.mouse.jsWrapper.JsMurmur;
import tk.mii8303.mouse.mouse.mouse;
import tk.mii8303.mouse.util.Util;

public class Plugin
{
  private Context cx;
  public Scriptable scope;
  private String fileContents;
  private Runnable runnable;
  private Thread thread;
  private JsMurmur murmur;
  private JsMouse Jmouse;
  private String firstShimString=null;
  private String pluginName="";
  private JsHttp jhttp;
  public Plugin(File file,String name)
  {
    try
    {
      fileContents = Util.readFile(file);
      pluginName = name;
    }
    catch (IOException e)
    {
      mouse.log.warn("Cannot read plugin: %s",file.getAbsolutePath());
      e.printStackTrace();
    }
  }
  public void start()
  {
    runnable = new Runnable()
    {
      @Override
      public void run()
      {
        cx = Context.enter();
        scope = cx.initStandardObjects();
        murmur = new JsMurmur();
        Jmouse = new JsMouse();
        jhttp = new JsHttp(pluginName);
        Object jsIce = Context.javaToJS(mouse.ice,scope);//populate namespace
        ScriptableObject.putProperty(scope, "ice",jsIce);
        Object jsLog = Context.javaToJS(mouse.log, scope);
        ScriptableObject.putProperty(scope, "console", jsLog);
        Object jsMurmur = Context.javaToJS(murmur, scope);
        ScriptableObject.putProperty(scope,"_murmur",jsMurmur);
        Object jsName = Context.javaToJS(pluginName,scope);
        ScriptableObject.putProperty(scope,"pluginName",jsName);
        Object jsMouse = Context.javaToJS(Jmouse,scope);
        ScriptableObject.putProperty(scope, "mouse", jsMouse);
        Object jsHttp  = Context.javaToJS(jhttp, scope);
        ScriptableObject.putProperty(scope, "_Http", jsHttp);
        try
        {
          cx.evaluateString(scope,firstShim() + fileContents, "index.js", 0, null);
        }
        catch(RhinoException e)
        {
          mouse.log.error(String.format("%s:%s:%s - %s\n%s\n%s",pluginName,e.lineNumber(),e.columnNumber(),e.details(),e.lineSource(),e.getScriptStackTrace()));
          e.printStackTrace();
        }
      }
    };
    thread = new Thread(runnable);
    thread.start();
  }
  private String firstShim()
  {
    if(firstShimString!=null)
    {
      return firstShimString;
    }
    else
    {
      try
      {
        firstShimString = Util.readResource("/shim.js").replace("\n", "").replace("\r","");
        firstShimString+="\n";
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
      return firstShimString;
    }
  }
}
