package tk.mii8303.mouse.ice;

public class ice
{
  public static Ice.Communicator iceCommunicator;
  public static Ice.ObjectPrx proxy;
  public static Murmur.MetaPrx murmur;
  public static String proxyString = "";
  public static Ice.ObjectAdapter callbackAdapter;
  public boolean start(String host, String port, String secret)
  {
    try
    {
      Ice.Properties properties = Ice.Util.createProperties();
      Ice.InitializationData initData = new Ice.InitializationData();
      
      properties.setProperty("Ice.ImplicitContext", "Shared");
      properties.setProperty("Ice.Default.EncodingVersion","1.0");
      
      initData.properties = properties;
      
      iceCommunicator = Ice.Util.initialize(initData);
      if(!secret.isEmpty())
      {
        iceCommunicator.getImplicitContext().put("secret",secret);
      }
      
      if(!port.isEmpty())
      {
        port = String.format("-p %s",port);
      }
      proxyString = String.format("Meta:tcp -h %s -t 2500 %s",host,port);
      proxy = iceCommunicator.stringToProxy(proxyString);
      
      murmur = Murmur.MetaPrxHelper.checkedCast(proxy);
      
      callbackAdapter = iceCommunicator.createObjectAdapterWithEndpoints("Callback.Client","tcp");
      return true;
    }
    catch(Exception e)
    {
      e.printStackTrace();
      return false; 
    }
    
  }
}