package tk.mii8303.mouse.http;

import org.eclipse.jetty.server.Server;

public class Http {
	Server server;
	HttpHandler handler;
	public void start() throws Exception
	{
		server = new Server(8080);
		handler = new HttpHandler();
		server.setHandler(handler);
		server.start();
	}
}
