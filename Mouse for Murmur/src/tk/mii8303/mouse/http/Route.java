package tk.mii8303.mouse.http;

import java.util.regex.Pattern;

public class Route
{
  public Pattern pattern;
  public RouteCallback callback;
  
  public Route(Pattern _pattern, RouteCallback _callback)
  {
    pattern = _pattern;
    callback = _callback;
  }
  public Route(String sPattern, RouteCallback _callback)
  {
    callback = _callback;
    pattern = Pattern.compile(sPattern);
  }
}
