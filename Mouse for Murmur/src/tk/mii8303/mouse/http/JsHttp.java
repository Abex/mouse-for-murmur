package tk.mii8303.mouse.http;

import java.util.ArrayList;

import tk.mii8303.mouse.mouse.mouse;

public class JsHttp
{
  private String pluginname;
  public JsHttp(String _pluginname)
  {
    pluginname=_pluginname;
  }
  public void get(String route,RouteCallback callback)
  {
    ArrayList<Route> routes;
    if(!mouse.http.handler.baseRoutes.containsKey(pluginname))
    {
      routes = new ArrayList<Route>();
    }
    else
    {
      routes = mouse.http.handler.baseRoutes.get(pluginname);
    }
    routes.add(new Route(route,callback));
    mouse.http.handler.baseRoutes.put(pluginname, routes);
  }
}
