package tk.mii8303.mouse.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import tk.mii8303.mouse.mouse.mouse;

public class HttpHandler extends AbstractHandler
{
  public HashMap<String,ArrayList<Route>> baseRoutes = new HashMap<String,ArrayList<Route>>();
  private Pattern splitter = Pattern.compile("^/([^/]*)/(.*)$");
	@Override
	public void handle(String target, Request bReq, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException
	{
	  Matcher urlParts = splitter.matcher(target);
	  urlParts.find();
	  String plugin = urlParts.group(1);
	  String path = "/"+urlParts.group(2);
	  if(baseRoutes.containsKey(plugin))
	  {
	    ArrayList<Route> Routes = baseRoutes.get(plugin);
	    for(Route route : Routes)
	    {
	      if(route.pattern.matcher(path).matches())
	      {
	        mouse.log.log(path);
	        if(route.callback.callback(target, bReq, req, res))
	        {
	          break;
	        }
	      }
	    }
	  }
	}
}
