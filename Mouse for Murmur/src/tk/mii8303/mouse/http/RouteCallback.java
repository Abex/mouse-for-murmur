package tk.mii8303.mouse.http;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

public interface RouteCallback
{
  public Boolean callback(String target, Request bReq, HttpServletRequest req, HttpServletResponse res);
}
