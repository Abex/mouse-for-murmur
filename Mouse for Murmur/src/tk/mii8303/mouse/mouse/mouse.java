package tk.mii8303.mouse.mouse;

import java.io.File;

import Ice.IntHolder;
import Ice.StringHolder;
import tk.mii8303.mouse.http.Http;
import tk.mii8303.mouse.ice.ice;
import tk.mii8303.mouse.util.Logger;
import tk.mii8303.mouse.pluginLoader.pluginLoader;

public class mouse
{
  public static Logger log;
  public static ice ice;
  public static Http http;
  /**
   * @param args
   */
  @SuppressWarnings({ "static-access" })
  public static void main(String[] args)
  {
    log = new Logger((short) 4,true);
    log.log("Starting Mouse For Mumble");
    ice = new ice();
    if(!ice.start("127.0.0.1","6502","seecretupdate")) // ip, port and secret
    {
      log.error("Cannot connect to server!");
    }
    StringHolder versionText = new StringHolder();
    IntHolder kill=new IntHolder();
    
    ice.murmur.getVersion(kill,kill,kill,versionText);
    log.log("Connected to Murmur %s",versionText.value);
    
    http = new Http();
    try
    {
		http.start();
	}
    catch (Exception e)
    {
		e.printStackTrace();
	}
    
    log.log("Starting plugins");
    pluginLoader.loadPlugins(new File("./plugins/"));
    pluginLoader.startPlugins();
    ice.iceCommunicator.waitForShutdown();
    log.log("Exiting");
  }

}
