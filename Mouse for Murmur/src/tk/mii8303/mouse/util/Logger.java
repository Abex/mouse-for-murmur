
package tk.mii8303.mouse.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger
{
  private short logLevel=0;
  private boolean timestamps=false;
  private DateFormat timestampFormat;
  /**
   * 
   * @param _logLevel 0- No Messages AT ALL 1- Critical Errors 2- Warnings 3-Normal 4-debug
   * @param _timestamps
   */
  public Logger(short _logLevel,boolean _timestamps)
  {
    logLevel=_logLevel;
    timestamps = _timestamps;
    if(timestamps)
    {
      timestampFormat = new SimpleDateFormat("HH:mm:ss MM/dd/YY");
    }
  }
  public Logger(short _logLevel,DateFormat timestampFormat)
  {
    logLevel=_logLevel;
    timestamps = true;
  }
  public void debug(String format, Object... args)
  {
    if(logLevel>=4)
    {
      System.out.println(prepend("DEBUG")+String.format(format,args));
    }
  }
  public void log(String format, Object... args)
  {
    if(logLevel>=3)
    {
      System.out.println(prepend("INFO")+String.format(format,args));
    }
  }
  public void warn(String format, Object... args)
  {
    if(logLevel>=2)
    {
      System.out.println(prepend("WARNING")+String.format(format,args));
    }
  }
  public void error(String format, Object... args)
  {
    if(logLevel>=4)
    {
      System.out.println(prepend("ERROR")+String.format(format,args));
    }
  }
  private String prepend(String source)
  {
    if(timestamps)
    {
      return String.format("[%s - %s] ",timestampFormat.format(new Date()),source);
    }
    else
    {
      return String.format("[%s] ",source);
    }
  }
}
