package tk.mii8303.mouse.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import tk.mii8303.mouse.mouse.mouse;

public class Util
{
  public static String readFile(File file) throws IOException 
  {
    StringBuilder fileContents = new StringBuilder((int)file.length());
    Scanner scanner = new Scanner(file);
    String lineSeparator = System.getProperty("line.separator");

    try 
    {
      while(scanner.hasNextLine()) 
      {        
        fileContents.append(scanner.nextLine() + lineSeparator);
      }
      return fileContents.toString();
    } 
    finally 
    {
      scanner.close();
    }
  }
  public static String readResource(String name) throws IOException
  {
    String returnString="";
    InputStream stream = mouse.class.getResourceAsStream(""+name);
    if(stream==null)
    {
      throw new IOException("Resource not found");
    }
    Scanner input=null;
    input = new Scanner(stream);
    while(input.hasNextLine())
    {
      returnString+=input.nextLine();
    }
    input.close();
    return returnString;
  }
}
