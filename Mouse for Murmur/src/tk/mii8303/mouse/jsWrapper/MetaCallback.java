package tk.mii8303.mouse.jsWrapper;

import Ice.Current;
import Murmur.ServerPrx;
import Murmur._MetaCallbackDisp;

public class MetaCallback extends _MetaCallbackDisp
{
  private static final long serialVersionUID = -6641803323112421708L;
  private IjsCallback callback;
  public MetaCallback(IjsCallback _callback)
  {
    callback=_callback; 
  }
  @Override
  public void started(ServerPrx srv, Current __current)
  {
    callback.callback(srv);
  }
  @Override
  public void stopped(ServerPrx srv, Current __current)
  {
  }
}
