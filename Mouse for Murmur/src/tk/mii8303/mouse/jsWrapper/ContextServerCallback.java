package tk.mii8303.mouse.jsWrapper;

import Ice.Current;
import Murmur.User;
import Murmur._ServerContextCallbackDisp;

public class ContextServerCallback extends _ServerContextCallbackDisp
{
  private IjsCallback4Arg callback;
  private static final long serialVersionUID = -5603592864357478110L;
  public ContextServerCallback(IjsCallback4Arg _callback)
  {
    callback = _callback;
  }
  @Override
  public void contextAction(String action, User usr, int session, int channelid, Current __current)
  {
    callback.callback(action, usr, session, channelid);
  }
}
