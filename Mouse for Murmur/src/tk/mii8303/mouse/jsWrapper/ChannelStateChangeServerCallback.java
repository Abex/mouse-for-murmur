package tk.mii8303.mouse.jsWrapper;

import Ice.Current;
import Murmur.Channel;
import Murmur.TextMessage;
import Murmur.User;
import Murmur._ServerCallbackDisp;

public class ChannelStateChangeServerCallback extends _ServerCallbackDisp
{
  private static final long serialVersionUID = -1568941875209900925L;
  private IjsCallback callback;
  public ChannelStateChangeServerCallback(IjsCallback _callback)
  {
    callback = _callback;
  }
  @Override
  public void userConnected(User state, Current __current)
  {
  }
  @Override
  public void userDisconnected(User state, Current __current)
  {
  }
  @Override
  public void userStateChanged(User state, Current __current)
  {
  }
  @Override
  public void userTextMessage(User state, TextMessage message, Current __current)
  { 
  }
  @Override
  public void channelCreated(Channel state, Current __current)
  { 
  }
  @Override
  public void channelRemoved(Channel state, Current __current)
  { 
  }
  @Override
  public void channelStateChanged(Channel state, Current __current)
  {
    callback.callback(state); 
  }
}
