package tk.mii8303.mouse.jsWrapper;

public interface IjsCallback
{
  public void callback(Object args);
  public String getPlugin();
}
