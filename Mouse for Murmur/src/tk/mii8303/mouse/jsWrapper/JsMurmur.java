package tk.mii8303.mouse.jsWrapper;

import java.util.ArrayList;
import java.util.Map;

import Ice.IntHolder;
import Ice.ObjectPrx;
import Ice.StringHolder;
import Murmur.Channel;
import Murmur.InvalidCallbackException;
import Murmur.InvalidSecretException;
import Murmur.MetaCallbackPrx;
import Murmur.MetaCallbackPrxHelper;
import Murmur.ServerBootedException;
import Murmur.ServerCallbackPrx;
import Murmur.ServerCallbackPrxHelper;
import Murmur.ServerContextCallbackPrx;
import Murmur.ServerContextCallbackPrxHelper;
import Murmur.ServerPrx;
import Murmur.User;
import tk.mii8303.mouse.ice.ice;

public class JsMurmur
{
  private ArrayList<MetaCallbackPrx> metaCallbacks = new ArrayList<MetaCallbackPrx>();
  private ArrayList<ServerCallbackPrx> serverCallbacks = new ArrayList<ServerCallbackPrx>();
  private ArrayList<ServerContextCallbackPrx> serverContextCallbacks = new ArrayList<ServerContextCallbackPrx>();
  public int _forEachServer(IjsCallback callback)
  {
    ServerPrx[] servers;
    try
    {
      servers = ice.murmur.getBootedServers();
      for(ServerPrx server : servers)
      {
        callback.callback(server); 
      }
      ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new MetaCallback(callback));
      ice.callbackAdapter.activate();
      MetaCallbackPrx cb = MetaCallbackPrxHelper.uncheckedCast(newCallback);
      int currentIndex = metaCallbacks.size();
      metaCallbacks.add(currentIndex, cb);
      ice.murmur.addCallback(cb);
      return currentIndex;
    }
    catch (InvalidSecretException | InvalidCallbackException e)
    {
      e.printStackTrace();
    }
    return -1;
  }
  public int _onStarted(IjsCallback callback)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new StartMetaCallback(callback));
    ice.callbackAdapter.activate();
    MetaCallbackPrx cb = MetaCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = metaCallbacks.size();
    metaCallbacks.add(currentIndex, cb);
    try
    {
      ice.murmur.addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onStopped(IjsCallback callback)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new StopMetaCallback(callback));
    ice.callbackAdapter.activate();
    MetaCallbackPrx cb = MetaCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = metaCallbacks.size();
    metaCallbacks.add(currentIndex, cb);
    try
    {
      ice.murmur.addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerConnect(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new ConnectServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerDisconnect(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new DisconnectServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerStateChanged(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new StateChangeServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerChannelCreated(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new ChannelCreatedServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerChannelRemoved(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new ChannelRemovedServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerChannelStateChanged(IjsCallback callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new ChannelStateChangeServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _onServerUserTextMessage(IjsCallback2Arg callback,int server)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new UserTextMessageServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerCallbackPrx cb = ServerCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverCallbacks.size();
    serverCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addCallback(cb);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public int _contextCallback(IjsCallback4Arg callback,int server, int session, String action, String text, int _currentIndex)
  {
    ObjectPrx newCallback = ice.callbackAdapter.addWithUUID(new ContextServerCallback(callback));
    ice.callbackAdapter.activate();
    ServerContextCallbackPrx cb = ServerContextCallbackPrxHelper.uncheckedCast(newCallback);
    int currentIndex = serverContextCallbacks.size();
    serverContextCallbacks.add(currentIndex,cb);
    try
    {
      ice.murmur.getServer(server).addContextCallback(session, action, text, cb, _currentIndex);
    }
    catch (InvalidCallbackException | InvalidSecretException  | ServerBootedException e)
    {
      e.printStackTrace();
      return -1;
    }
    return currentIndex;
  }
  public void _removeContextCallback(int callback, int server)
  {
    try
    {
      ice.murmur.getServer(server).removeContextCallback(serverContextCallbacks.get(callback));
    } catch (InvalidCallbackException | InvalidSecretException | ServerBootedException e)
    {
      e.printStackTrace();
    }
  }
  public void _removeServerCallback(int callback, int server)
  {
    try
    {
      ice.murmur.getServer(server).removeCallback(serverCallbacks.get(callback));
    }
    catch (InvalidCallbackException | InvalidSecretException | ServerBootedException e)
    {
      e.printStackTrace();
    }
  }
  public void _removeMetaCallback(int callback)
  {
    try
    {
      ice.murmur.removeCallback(metaCallbacks.get(callback));
    }
    catch (InvalidCallbackException | InvalidSecretException e)
    {
      e.printStackTrace();
    }
  }
  public int[] _getVersionNumbers()
  {
    int[] ret = new int[3];
    IntHolder a = new IntHolder();
    IntHolder b = new IntHolder();
    IntHolder c = new IntHolder();
    StringHolder text = new StringHolder();
    ice.murmur.getVersion(a, b, c, text);
    ret[0]=a.value;
    ret[1]=b.value;
    ret[2]=c.value;
    return ret;
  }
  public String _getVersionString()
  {
    IntHolder a = new IntHolder();
    IntHolder b = new IntHolder();
    IntHolder c = new IntHolder();
    StringHolder text = new StringHolder();
    ice.murmur.getVersion(a, b, c, text);
    return text.value;
  }
  public Object _getDefaultConf()
  {
    try
    {
      Map<String,String>map = ice.murmur.getDefaultConf();
      return new ForMap(map);
    }
    catch (InvalidSecretException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  public Object _getAllConfServer(int serverId)
  {
    try
    {
      Map<String,String>map = ice.murmur.getServer(serverId).getAllConf();
      return new ForMap(map);
    }
    catch (InvalidSecretException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  public Object  _getUsersServer(int serverId)
  {
    try
    {
      Map<Integer ,User>map = ice.murmur.getServer(serverId).getUsers();
      return new ForMap(map);
    }
    catch (InvalidSecretException | ServerBootedException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  public Object  _getChannelsServer(int serverId)
  {
    try
    {
      Map<Integer ,Channel>map = ice.murmur.getServer(serverId).getChannels();
      return new ForMap(map);
    }
    catch (InvalidSecretException | ServerBootedException e)
    {
      e.printStackTrace();
    }
    return null;
  }
}
