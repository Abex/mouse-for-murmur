package tk.mii8303.mouse.jsWrapper;

import java.util.Map;

public class ForMap
{
  @SuppressWarnings("rawtypes")
  private Map map;
  public ForMap(@SuppressWarnings("rawtypes") Map _map)
  {
    map=_map;
  }
  public Object get(Object k)
  {
    return map.get(k);
  }
  public void foreach(IjsCallback cb)
  {
    for(Object k : map.keySet())
    {
      cb.callback(k);
    }
  }
}
