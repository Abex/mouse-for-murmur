package tk.mii8303.mouse.jsWrapper;

public interface IjsCallback2Arg
{
  public void callback(Object a1, Object a2);
  public String getPlugin();
}
